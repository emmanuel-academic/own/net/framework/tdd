﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Evoluteal.TDD.Business.Exercises.Tests
{
    [TestClass()]
    public class FizzBuzzTests
    {
        //
        //Desarrollar una función que
        //    imprima Fizz si el número es divisible por 3,
        //    Buzz si el número es divisible por 5 y FizzBuzz 
        //    si es divisible por ambos, en cualquier otro caso debe mostrar el número.

        /// 
        /*
         * si le pasamos el numero 1 debe devolver 1
         * si le pasamos el numero 2 debe devolver 2
         * si le pasamos el numero 3 debe devolver FIZZ
         * si le pasamos el numero 4 debe devolver 4
         * si le pasamos el numero 5 debe devolver BUZZ
         * si le pasamos el numero 6 debe devolver FIZZ
         *  si le pasamos el numero 7 debe devolver 7
         *   si le pasamos el numero 8 debe devolver 8
         *   si le pasamos el numero 9 debe devolver FIZZ
         *    si le pasamos el numero 10 debe devolver BUZZ
         *    
         *     si le pasamos el numero 15 debe devolver FIZZBUZZ
         *     si le pasamos el numero 30 debe devolver FIZZBUZZ
         *   si le pasamos el numero 45 debe devolver FIZZBUZZ
         */

        public void ProbarFizzBuzz(string valorEsperado, int ValorActual)
        {
            FizzBuzz lab = new FizzBuzz();
            Assert.AreEqual(valorEsperado, lab.DoFizzBuzz(ValorActual));
        }

        [TestMethod]
        public void TestMethod1() => ProbarFizzBuzz(valorEsperado: "1", ValorActual: 1);

        [TestMethod]
        public void TestMethod2() => ProbarFizzBuzz(valorEsperado: "2", ValorActual: 2);

        [TestMethod]
        public void TestMethod3() => ProbarFizzBuzz(valorEsperado: "FIZZ", ValorActual: 3);

        [TestMethod]
        public void TestMethod4() => ProbarFizzBuzz(valorEsperado: "4", ValorActual: 4);

        [TestMethod]
        public void TestMethod5() => ProbarFizzBuzz(valorEsperado: "BUZZ", ValorActual: 5);

        [TestMethod]
        public void TestMethod6() => ProbarFizzBuzz(valorEsperado: "FIZZ", ValorActual: 6);

        [TestMethod]
        public void TestMethod7() => ProbarFizzBuzz(valorEsperado: "7", ValorActual: 7);

        [TestMethod]
        public void TestMethod8() => ProbarFizzBuzz(valorEsperado: "8", ValorActual: 8);

        [TestMethod]
        public void TestMethod9() => ProbarFizzBuzz(valorEsperado: "FIZZ", ValorActual: 9);

        [TestMethod]
        public void TestMethod10() => ProbarFizzBuzz(valorEsperado: "BUZZ", ValorActual: 10);

        [TestMethod]
        public void TestMethod15() => ProbarFizzBuzz(valorEsperado: "FIZZBUZZ", ValorActual: 15);

        [TestMethod]
        public void TestMethod30() => ProbarFizzBuzz(valorEsperado: "FIZZBUZZ", ValorActual: 30);

        [TestMethod]
        public void TestMethod45() => ProbarFizzBuzz(valorEsperado: "FIZZBUZZ", ValorActual: 45);
    }
}