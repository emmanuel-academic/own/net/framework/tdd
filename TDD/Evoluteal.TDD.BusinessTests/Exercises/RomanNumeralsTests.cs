﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Evoluteal.TDD.Business.Exercises.Tests
{
    [TestClass()]
    public class RomanNumeralsTests
    {
        /*
           * Desarrollar una función que convierta de 
           * números arábigo a números romano
           * 
           * CASOS DE PRUEBA
           * 
           * 1 = I
           * 2 = II
           * 3 = III
           * 4 = IV
           * 5 = V
           * 6 = VI
           * 7 = VII
           * 8 = VIII
           * 9 = IX
           * 10 = X
           * .
           * 20 = XX
           * 21 = XXI
           * .
           * .
           * 30 = XXX
           * 31 = XXXI
           * .
           * .
           * 40 = XL
           * 41 = XLI
           * .
           * .
           * 50 = L
           * 51 = LI
           * .
           * .
           * 60 = LX
           * 61 = LXI
           * .
           * .
           * 70 = LXX
           * 71 = LXXI
           * .
           * .
           * 80 = LXXX
           * 81 = LXXXI
           * .
           * .
           * 90 = XC
           * 90 = XCI
           * .
           * .
           * 100 = C
           * 101 = CI
           * 
           */

        public void decimalEnRomano(int iDecimal, string strRomano)
        {
            RomanNumerals romano = new RomanNumerals();
            Assert.AreEqual(romano.ToRoman(iDecimal), strRomano);
        }

        [TestMethod]
        public void TestMethod1() => decimalEnRomano(1, "I");

        [TestMethod]
        public void TestMethod2() => decimalEnRomano(2, "II");

        [TestMethod]
        public void TestMethod3() => decimalEnRomano(3, "III");

        [TestMethod]
        public void TestMethod4() => decimalEnRomano(4, "IV");

        [TestMethod]
        public void TestMethod5() => decimalEnRomano(5, "V");

        [TestMethod]
        public void TestMethod6() => decimalEnRomano(6, "VI");

        [TestMethod]
        public void TestMethod7() => decimalEnRomano(7, "VII");

        [TestMethod]
        public void TestMethod8() => decimalEnRomano(8, "VIII");

        [TestMethod]
        public void TestMethod9() => decimalEnRomano(9, "IX");

        [TestMethod]
        public void TestMethod10() => decimalEnRomano(10, "X");

        [TestMethod]
        public void TestMethod11() => decimalEnRomano(11, "XI");

        [TestMethod]
        public void TestMethod12() => decimalEnRomano(12, "XII");

        [TestMethod]
        public void TestMethod20() => decimalEnRomano(20, "XX");

        [TestMethod]
        public void TestMethod21() => decimalEnRomano(21, "XXI");

        [TestMethod]
        public void TestMethod24() => decimalEnRomano(24, "XXIV");

        [TestMethod]
        public void TestMethod30() => decimalEnRomano(30, "XXX");

        [TestMethod]
        public void TestMethod31() => decimalEnRomano(31, "XXXI");

        [TestMethod]
        public void TestMethod40() => decimalEnRomano(40, "XL");

        [TestMethod]
        public void TestMethod41() => decimalEnRomano(41, "XLI");

        [TestMethod]
        public void TestMethod50() => decimalEnRomano(50, "L");

        [TestMethod]
        public void TestMethod51() => decimalEnRomano(51, "LI");

        [TestMethod]
        public void TestMethod60() => decimalEnRomano(60, "LX");

        [TestMethod]
        public void TestMethod61() => decimalEnRomano(61, "LXI");

        [TestMethod]
        public void TestMethod70() => decimalEnRomano(70, "LXX");

        [TestMethod]
        public void TestMethod71() => decimalEnRomano(71, "LXXI");

        [TestMethod]
        public void TestMethod80() => decimalEnRomano(80, "LXXX");

        [TestMethod]
        public void TestMethod81() => decimalEnRomano(81, "LXXXI");

        [TestMethod]
        public void TestMethod90() => decimalEnRomano(90, "XC");

        [TestMethod]
        public void TestMethod91() => decimalEnRomano(91, "XCI");

        [TestMethod]
        public void TestMethod100() => decimalEnRomano(100, "C");

        [TestMethod]
        public void TestMethod101() => decimalEnRomano(101, "CI");

        [TestMethod]
        public void TestMethod390() => decimalEnRomano(390, "CCCXC");

        [TestMethod]
        public void TestMethod399() => decimalEnRomano(399, "CCCXCIX");

        [TestMethod]
        public void TestMethod400() => decimalEnRomano(400, "CD");

        [TestMethod]
        public void TestMethod401() => decimalEnRomano(401, "CDI");

        [TestMethod]
        public void TestMethod500() => decimalEnRomano(500, "D");

        [TestMethod]
        public void TestMethod501() => decimalEnRomano(501, "DI");
    }
}