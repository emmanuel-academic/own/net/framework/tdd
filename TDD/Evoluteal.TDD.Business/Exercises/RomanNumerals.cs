﻿using System.Collections.Generic;
using System.Text;

namespace Evoluteal.TDD.Business.Exercises
{
    public class RomanNumerals
    {
        public string ToRoman(int numero)
        {
            StringBuilder resultado = new StringBuilder();
            int contador = numero;

            Dictionary<int, string> simbolos = new Dictionary<int, string>();
            simbolos.Add(key: 500, value: "D");
            simbolos.Add(key: 400, value: "CD");
            simbolos.Add(key: 100, value: "C");
            simbolos.Add(key: 90, value: "XC");
            simbolos.Add(key: 50, value: "L");
            simbolos.Add(key: 40, value: "XL");
            simbolos.Add(key: 10, value: "X");
            simbolos.Add(key: 9, value: "IX");
            simbolos.Add(key: 5, value: "V");
            simbolos.Add(key: 4, value: "IV");
            simbolos.Add(key: 1, value: "I");

            foreach (var simbolo in simbolos)
            {
                while (contador >= simbolo.Key)
                {
                    resultado.Append(simbolo.Value);
                    contador = contador - simbolo.Key;
                }
            }
            return resultado.ToString();
        }
    }
}
