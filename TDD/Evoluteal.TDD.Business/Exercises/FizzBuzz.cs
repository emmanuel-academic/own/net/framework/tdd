﻿namespace Evoluteal.TDD.Business.Exercises
{
    public class FizzBuzz
    {
        public string DoFizzBuzz(int numero)
        {
            if (EsDivisiblePor(numero, 3) && EsDivisiblePor(numero, 5))
            {
                return "FIZZBUZZ";
            }
            if (EsDivisiblePor(numero, 3))
            {
                return "FIZZ";
            }
            else if (EsDivisiblePor(numero, 5))
            {
                return "BUZZ";
            }

            return numero.ToString();
        }

        public bool EsDivisiblePor(int dividendo, int divisor)
        {
            return dividendo % divisor == 0;
        }
    }
}

